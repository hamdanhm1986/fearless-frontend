function createCard(name, description, pictureUrl, formattedStart, formattedEnd, locationName) {
  return `
    <div class="card shadow-sm p-3 mb-5 bg-body rounded">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
        <p class="card-text">${description}</p>
        <div class="card-footer">${formattedStart}-${formattedEnd}</div>
        <div class="card-footer"></div>
      </div>
    </div>
  `;
}

window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
    } else {
      const data = await response.json();

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
          const details = await detailResponse.json();

          const name = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const startDate = new Date (details.conference.starts)
          const endDate = new Date (details.conference.ends)
          const locationName = details.conference.location.name
          const formattedStart = startDate.toLocaleDateString();
          const formattedEnd = endDate.toLocaleDateString();

          const html = createCard(name, description, pictureUrl, formattedStart, formattedEnd, locationName );

          const column = document.createElement('div');
          column.classList.add('col');
          column.innerHTML = html;
          const row = document.querySelector('.row');
          row.appendChild(column);
        }
      }

    }
   catch (e) {
    displayAlert(e.message, 'danger');
  }
});
